from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import logging

logger = logging.getLogger(__name__)

Base = declarative_base()

class CustomerContact(Base):
    __tablename__ = 'customer_contact'
    
    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False)
    subject = Column(String, nullable=False)
    contact = Column(String)
    company = Column(String)
    phone = Column(String)
    company_size = Column(Integer)
    


class DatabaseManager:
    def __init__(self, db_url: str) -> None:
        self.engine = create_engine(db_url)
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)
        
    def compare_data_with_database(self, from_address: str, subject: str) -> bool:
        session = self.Session()
        
        result = session.query(CustomerContact).filter_by(email=from_address, subject=subject).first()
        session.close()
       
        return bool(result)
    
    def save_data_to_database(self, from_address: str, subject: str, contact_info: dict) -> None:
        try:
            session = self.Session()
            
            logger.info(f"{contact_info.get('Contact', ''), contact_info.get('Company', ''), contact_info.get('Phone', ''), contact_info.get('Company Size ')}")
            new_email_data = CustomerContact(email=from_address,
                                         subject=subject,
                                         contact=contact_info.get('Contact', ''),
                                         company=contact_info.get('Company', ''),
                                         phone=contact_info.get('Phone', ''),
                                         company_size=contact_info.get('Company Size ', ''))
            session.add(new_email_data)
            session.commit()
        except Exception as e:
            logger.error(f"Error saving data to database: {e}")
            session.rollback()  # Rollback the transaction in case of an exception
        finally:
            session.close()
