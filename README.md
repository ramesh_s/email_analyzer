# Configuration Readme

 This project is designed to analyze and process emails from a specified IMAP server. It extracts information such as sender details, subject, and payload from emails, then parses necessary  information from the plain text payload. The script further filters emails based on target addresses and updates a database with relevant information.

# Installation

To set up the project locally, follow these steps:

Clone the repository:

`git clone https://gitlab.com/ramesh_s/email_analyzer.git`

Navigate to the project directory:

`cd data_matrix`

Install dependencies:

`pip install -r requirements.txt`

 # Docker

You can also use Docker Compose to run your application and manage dependencies.
Use the following command:

`docker-compose up`

#  Environment Variables

Create a .env file in the same directory as the script with the following content:

```
IMAP_SERVER=<your_imap_server> #
IMAP_PORT=993 # 
EMAIL_USERNAME=<your_email_username>
EMAIL_PASSWORD=<your_email_password>
TARGET_ADDRESSES=<comma_separated_target_emails>
FOLDER=<your_email_folder>
DB_URL=<your_database_url> 
```

# Important

The database must contain the sender's address and subject
before the start of project

# Message Format

~~A new lead contacted you

Dear Ramesh,

A new potential customer is waiting for you to follow up.

Referral #123ABC Information:

Contact: Daniel Dupont

Company: Dupont Inc.

Phone: +33 652 010203

Email: daniel@acme.com

Zip: 75015

Location: Paris

Interested in: Supply Chain Management

Company Size : 50

Best Regards,
Your Lead Provider~~




# Script Customization
Feel free to customize the script based on your specific requirements:

**Logging**: Adjust the logging configuration in the script (logging.basicConfig) to match your logging preferences.

**Database Manager:** If you are using a different type of database, modify the DB_URL variable in the .env file and update the DatabaseManager instantiation accordingly.

**Email Processing:** If additional processing or modifications are required for email content or lead contact information extraction, modify the corresponding methods within the EmailAnalyzer class.

**Error Handling:**
 Enhance or customize error handling  functions to suit your needs.
 


