from contextlib import contextmanager
import imaplib
import email
from email.header import decode_header
from dataclasses import dataclass
import re
from database import DatabaseManager
import logging
import os
from dotenv import load_dotenv



logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s]: %(message)s",
        handlers=[
            logging.StreamHandler(),
            logging.FileHandler("email_analyzer.log")
        ]
    )
logger = logging.getLogger(__name__)


@dataclass
class EmailAnalyzer:
    server: str
    port: int
    username: str
    password: str
    target_addresses: list[str]
    folder: str
    db_manager: DatabaseManager
    
    
    @contextmanager
    def connect(self):

        """ The context manager sets up a secure IMAP connection with the specified server,
        port, username, password, and initial folder. It yields the IMAP connection,
        allowing operations to be performed within the context. Upon exiting the context,
        it logs out from the IMAP server."""
        
        mail = imaplib.IMAP4_SSL(self.server, self.port, timeout=10)
        try:
            logger.info("IMAP connection...")
            mail.login(self.username, self.password)
            mail.select(self.folder)
            yield mail
        except Exception as e:
            logger.error(f"Error during IMAP connection: {e}")
        finally:
            logger.info("IMAP disconnected")
            mail.logout()
    
    def analyze_email_message(self, raw_email):

        """ The function parses the raw email message and extracts information such as
    the sender's email address, the decoded subject, and the plain text payload.
    It handles multipart emails and retrieves the text/plain payload when available."""
        
        msg = email.message_from_bytes(raw_email)
        from_address = msg.get('From')
        subject, encoding = decode_header(msg['Subject'])[0]
        subject = subject.decode(encoding or 'utf-8') if isinstance(subject, bytes) else subject
        
        payload =""
        if msg.is_multipart():
            for part in msg.walk():
                content_type = part.get_content_type()
                if content_type == 'text/plain':
                    payload = part.get_payload()
        else:
            payload = msg.get_payload()
   
        return from_address, subject, payload
    
   
    
    def parse_lead_contact_info(self, plain_text_payload: str) -> dict:
   
        """  Returns a dictionary containing parsed lead contact information.
            The function uses a case-insensitive and space-tolerant regular expression to extract
            specific fields ('Contact', 'Company', 'Phone', 'Company Size') and their corresponding values
            from the plain text payload. """
       
        lead_contact_info = {}
        matches = re.findall(r'(contact|company|phone|company\s*size ): (.+)',
                              plain_text_payload,
                                re.IGNORECASE)
        lead_contact_info.update(matches)
        
        
        return lead_contact_info
    
    
    def process_emails(self, mail):
        """This method retrieves all emails from the specified mailbox, analyzes their content,
        and updates the database with relevant information. It filters emails based on target addresses,
        checks if the data is correct in the database, and saves new data."""
        
        _, messages = mail.search(None, 'ALL')
        message_ids = messages[0].split()
        
        email_data = [
            (mail.fetch(message_id, '(RFC822)')[1][0][1])
            for message_id in message_ids
        ]
        
        for raw_email in email_data:
            from_address, subject, payload = self.analyze_email_message(raw_email)
           
            if all([from_address,subject,payload]):
                if from_address.split()[-1] in self.target_addresses:
                    
                    if self.db_manager.compare_data_with_database(from_address, subject):
                        
                        payload_info = self.parse_lead_contact_info(payload)
                        
                        self.db_manager.save_data_to_database(from_address, subject,payload_info)
            else:
                logger.error(f"Data is not found: From: {from_address}\nSubject: {subject}\nPayload: {payload}")
                

def main():

    load_dotenv()
    
    IMAP_SERVER = os.getenv("IMAP_SERVER")
    IMAP_PORT = int(os.getenv("IMAP_PORT", 993))
    EMAIL_USERNAME = os.getenv("EMAIL_USERNAME")
    EMAIL_PASSWORD = os.getenv("EMAIL_PASSWORD")
    TARGET_ADDRESSES = os.getenv("TARGET_ADDRESSES", "").split(",")
    FOLDER_NAME = os.getenv("FOLDER")
    DB_URL = os.getenv("DB_URL", "sqlite:///example.db")
    print(EMAIL_PASSWORD)
    try:
        analyzer = EmailAnalyzer(
            server=IMAP_SERVER,
            port=IMAP_PORT,
            username=EMAIL_USERNAME,
            password=EMAIL_PASSWORD,
            target_addresses=["<" + email + ">" for email in TARGET_ADDRESSES],
            folder=FOLDER_NAME,
            db_manager = DatabaseManager(db_url=DB_URL)
        ) 
        with analyzer.connect() as mail:
            analyzer.process_emails(mail)
            
    except Exception as e:
        logger.error(f'Error {e}')

if __name__ == "__main__":
    main()